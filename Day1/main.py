def is_integer(n):
    try:
        float(n)
    except ValueError:
        return False
    else:
        return float(n).is_integer()

def sanitazor(stringInput: str): 
    return stringInput.replace('three', "thre3hree").replace('two', "tw2wo").replace('six', "si6ix").replace('seven', "sev7ven").replace('one', "on1ne").replace('nine',"nin9ine").replace('four', "fou4our").replace('five', "fiv5ive").replace('eight', "eigh8eight");

def getFirstDigit(stringInput: str):
    for character in stringInput: 
        if is_integer(character):
            return character;
    return "";


total: int = 0
with open('./data', 'r') as fp:
    for line in fp : 
        ## Part 1
        #sanitazedString = line

        ## Part 2
        sanitazedString = sanitazor(line)
        
        total = total + int(getFirstDigit(sanitazedString) + getFirstDigit(sanitazedString[::-1]))

print(total)