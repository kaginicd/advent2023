import { readFile } from 'fs';

const getCardNumber = (line) => line.split(" ").map(elem => Number(elem)).filter(elem => elem !== 0)

//Part 1
const cardPointCount = (cards) => {
    if (cards.length === 0) return 0;
    
    let total = 1;
    if (cards.length === 1 ) {return 1;}
    if (cards.length > 1) {
        for(let i = 2; i <= cards.length; i++) {
            total *= 2;
        }
        return total;
    }
};

const cardWinCounter = (cards) => {

    let cardWinMap = new Map();

    for(let i = 0; i < cards.length; i++) {
        let cardCopy = 1;
        
        if (cardWinMap.get(i)) {
            cardCopy += cardWinMap.get(i)
        }
        
        cardWinMap.set(i, cardCopy);


        for(let j = 1; j <= cards[i].length; j++) {
            // console.log({i,j, cardWinMap, cardCopy})
            if (cardWinMap.get(i + j)) {
                cardWinMap.set(i + j, cardWinMap.get(i + j) + cardCopy)
            } else {
                cardWinMap.set(i + j, cardCopy)
            }
        }
    }

    return cardWinMap;
}


readFile("./data", (err, data) => {
    if (err) throw err;

    const inputData = data.toString().split("\n");

    let total = 0;
    let total2 = 0;
    const cardContained = [];

    inputData.forEach((line, index) => {
        const cardInfoExploded = line.split(":") 
        const cardString = cardInfoExploded[1].split("|")

        const cardWin = getCardNumber(cardString[0])
        // console.table(cardWin)
        const cardDraw = getCardNumber(cardString[1])
        // console.table(cardDraw)
        
        // Part 1
        cardContained.push(cardWin.map(card => cardDraw.includes(card) ? card : undefined).filter(elem => elem !== undefined))
    });

    cardContained.forEach((card) => total += cardPointCount(card))
    console.log(`Part 1 - ${total}`)

    cardWinCounter(cardContained).forEach((nbCopy) => total2 += nbCopy)
    console.log(`Part 2 - ${total2}`);
})