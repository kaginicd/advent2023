import { readFile } from 'fs';

function isNumber(charInput: string) { return !Number.isNaN(Number(charInput)) }

function isNotOut(col: number, row: number, limitCol: number, limitRow: number): boolean {
    return col >= 0 && col <= limitCol && row >= 0 && row <= limitRow;
}

const squareGearMap = new Map<string, number>()

function squareGear(inputData: string[], startColNumberPosition: number, endColNumberPosition: number , raw: number, maxColSize: number, maxRawSize: number, squareGear: Map<string, number>, num: number): boolean{
    const minimalCol: number = (startColNumberPosition - 1 >= 0) ? startColNumberPosition - 1 : startColNumberPosition;
    const maxCol: number = endColNumberPosition;

    const mininalRow: number = (raw - 1 >= 0) ? raw - 1 : raw;
    const maxRow: number = (raw + 1 < maxRawSize) ? raw + 1 : raw;
    
    for (let k = mininalRow; k <= maxRow; k++){
        for (let l = minimalCol; l <= maxCol; l ++) {
            if (isNotOut(l, k, maxColSize, maxRawSize)) {
                if (inputData[k][l] === "*") {
                    if (squareGear.get(JSON.stringify([k,l]))) {
                        const beforPositions: number = Number(squareGear.get(JSON.stringify([k,l])));
                        squareGear.set(JSON.stringify([k,l,"d"]), beforPositions * num)
                        squareGear.delete(JSON.stringify([k,l]))
                    } else {
                        squareGear.set(JSON.stringify([k,l]), num)
                    }
                    return true
                }
            }
        }
    }
    return false;
}

function squareSymboleCheck(inputData: string[], startColNumberPosition: number, endColNumberPosition: number , raw: number, maxColSize: number, maxRawSize: number): boolean{

    const minimalCol: number = (startColNumberPosition - 1 >= 0) ? startColNumberPosition - 1 : startColNumberPosition;
    const maxCol: number = endColNumberPosition;

    const mininalRow: number = (raw - 1 >= 0) ? raw - 1 : raw;
    const maxRow: number = (raw + 1 < maxRawSize) ? raw + 1 : raw;
    
    for (let k = mininalRow; k <= maxRow; k++){
        for (let l = minimalCol; l <= maxCol; l ++) {
            if (isNotOut(l, k, maxColSize, maxRawSize)) {
                if (!inputData[k][l].match(/\.|\d/gmi) && inputData[k][l] !== "*") {
                    return true
                }
            }
        }
    }
    return false;
}

readFile("./data", (err, data) => {
    if (err) throw err;

    const inputData: string[] = data.toString().split("\n");
    const numberRaw: number = inputData.length - 1;

    const numbers: number[] = []

    for (let i = 0; i <= numberRaw; i++) {
        const maxColSize = inputData[i].length - 1;
        let num: string = '';
        let startNumberPosition: number | undefined = undefined;
        let endNumberPosition: number | undefined = undefined;

        for (let j = 0; j <= maxColSize; j++) {

            if (isNumber(inputData[i][j])) {
                startNumberPosition= j;
                while (isNumber(inputData[i][j])) {
                    num += inputData[i][j];
                    j++;
                }
                endNumberPosition = j;

                if (squareSymboleCheck(inputData, startNumberPosition, endNumberPosition, i, maxColSize, numberRaw)) {
                    numbers.push(Number(num));
                }


                if (squareGear(inputData, startNumberPosition, endNumberPosition, i, maxColSize, numberRaw, squareGearMap, Number(num))) {

                }

                num = "";
                startNumberPosition = undefined;
                endNumberPosition = undefined;

            }
        }

    }

    let total: number = 0;
    // numbers.forEach(numb => total += numb);
    squareGearMap.forEach((numb, key) => {
        console.log(key)
        if (key.includes('"d"]')){
            total += numb
        }
    });
    console.log(total);
})
