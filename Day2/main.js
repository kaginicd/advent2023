const fs = require('fs');
const { totalmem } = require('os');

const limits = {
    red: 12,
    green: 13,
    blue: 14
};


const explodGameToArrays = (gamesInput) => gamesInput.split('; ').map((game) => {
    const subGame = {}
    game.split(', ').forEach(element => {
        const gg = element.split(" ");
        subGame[gg[1]] = gg[0];
    });

    return subGame;
})

const getGameId = (game) => game.replace("Game ", "");

const isGoodGame = (games) => 
    explodGameToArrays(games).filter((element) => (element["red"] && element["red"] > limits["red"])|| (element["green"] && element["green"] > limits["green"]) || (element["blue"] && element["blue"] > limits["blue"])).length === 0;


const getMaxCube = (cubeGames) => {
    const maxCubes = {red: 0, green: 0, blue: 0};
    cubeGames.forEach((cubes) => {
        maxCubes["red"] = Number((Number(cubes.red) > maxCubes.red) ? cubes.red : maxCubes.red);
        maxCubes["green"] = Number((Number(cubes.green) > maxCubes.green) ? cubes.green : maxCubes.green);
        maxCubes["blue"] = Number((Number(cubes.blue) > maxCubes.blue) ? cubes.blue : maxCubes.blue);
    })

    return maxCubes;
}

totalIDs = 0;
totalMultiply = 0;

fs.readFile("./data", (err, data) => {
    if (err) throw err;

    const inputData = data.toString().split("\n");
    inputData.forEach((line, _index) => {
        const explodedGame = line.split(": ");

        if (_index === 0) {
            console.table(getMaxCube(explodGameToArrays(explodedGame[1])));
        }


        // Part 1
        // if (isGoodGame(explodedGame[1]))
        //     totalIDs += Number(getGameId(explodedGame[0]).toString());


        //Part 2
        const maxCubes = getMaxCube(explodGameToArrays(explodedGame[1]));
        totalMultiply += maxCubes.blue * maxCubes.red * maxCubes.green;


    })

    // result Part 1 => 3059
    // console.log(totalIDs);

    // result Part 2 => 65371
    console.log(totalMultiply);
})
